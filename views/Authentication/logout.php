<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Author;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Author();
$status=$auth->log_out();
if($status){
    Message::message("You are successfully logged-out");
    return Utility::redirect('../../index.php');
}