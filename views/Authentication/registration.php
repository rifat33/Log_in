<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Author;
use App\Message\Message;
use App\Utility\Utility;



$authoration=new Author();
$exist=$authoration->prepare($_POST)->is_exist();

if($exist){
    Message::setMessage("<div class='alert alert-danger'><strong>Taken!</strong> Email has already been taken .</div>");
    return Utility::redirect('../../index.php');
}
else{
    $obj= new User();
    $obj->prepare($_POST)->store();
}
