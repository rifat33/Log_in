<?php
session_start();

include_once ('../../vendor/autoload.php');

use App\User\User;
use App\User\Author;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Author();
$status=$auth->prepare($_POST)->is_registered();
if($status){
    $_SESSION['user_email']=$_POST['email'];
    return Utility::redirect('../welcome.php');
}
else{
    Message::message("<div class='alert alert-danger'>Wrong Email or password !</div>");
    return Utility::redirect('../../index.php');

}
